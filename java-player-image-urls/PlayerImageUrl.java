import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

class PlayerImageUrl {
    public static String SecretValue    = "secretkeyvalue";
    public static String CloudFrontBase = "https://d1lor8s1noym9m.cloudfront.net/players";

    public static String getPlayerUrl(String leagueType, String playerId, String style) {
        // TODO Implement this method
    }

    public static void main(String args[]) {
        try {
            System.out.println(PlayerImageUrl.getPlayerUrl("nfl", "145267", "original"));
            System.out.println(PlayerImageUrl.getPlayerUrl("nfl", "851231", "original"));
            System.out.println(PlayerImageUrl.getPlayerUrl("nfl", "145267", "thumb"));
            System.out.println(PlayerImageUrl.getPlayerUrl("nfl", "851231", "thumb"));
            System.out.println(PlayerImageUrl.getPlayerUrl("mlb", "232210", "original"));
            System.out.println(PlayerImageUrl.getPlayerUrl("mlb", "601231", "original"));
            System.out.println(PlayerImageUrl.getPlayerUrl("mlb", "232210", "thumb"));
            System.out.println(PlayerImageUrl.getPlayerUrl("mlb", "601231", "thumb"));

        } catch(GeneralSecurityException e) {
            System.out.println("Error");
            System.out.println(e.getMessage());
        }
    }
}
