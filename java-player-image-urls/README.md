### Java Player Image URLs

Included in this directory is small Java program which is incomplete. To
complete this program, please implement the `getPlayerImage` static
class method as such:

1. First compute the SHA1 HMAC using the league type, player ID, and
   image style strings as input. These input should be concatenated
   together using a dash character. For example:
   `String input = "nfl-123456-original"`

2. Next, encode the resulting output bytes from the HMAC function into a
   hexadecimal string.

3. Finally, return the complete image URL using the `CloudFrontBase`
   static property string. The final URL should look like:
   `https://d1lor8s1noym9m.cloudfront.net/players/[leageType]/[imageStyle]/[hexadecimalHmacResult].png`

Also included in this directory is a `expected-results.txt` file. This
file shows what the expected output of this Java program should be and
can be used to verify the correctness of your implementation.
