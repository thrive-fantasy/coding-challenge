### JavaScript Player Image Loader

Included in this directory is a small web page and some
yet-to-be-implemented JavaScript. Your task is to implement the
remaining code to fetch the player image JSON file and load the images.
Once all the images are fully loaded, then they should be rendered to
the DOM.

Please implement the following:

1. The helper functions in `index.js`. The comments and functions names
   should clearly indicate what the function should do. All of these
   functions should return a `Promise` object.

2. Implement the `main` function which should consume and chain together
   the helper functions and the Promises they return.
