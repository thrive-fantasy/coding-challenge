var PLAYER_IMAGES = "https://s3.amazonaws.com/p2f-coding-challenge/player-images.json";

function fetchJson() {
    // TODO implement this function to retrieve JSON data from S3 via the
    // PLAYER_IMAGES URL using the fetch() function
    //
    // This function should return a Promise
}

function loadImages() {
    // TODO implement this function preload all images
    //
    // This function should return a Promise
}

function renderImages() {
    // TODO implement this function render images within the DOM element main#players
    //
    // This function should return a Promise
}

function main() {
    // TODO implement this function to consume Promises returned by the above
    // functions
}

// Execute main function now
main();
